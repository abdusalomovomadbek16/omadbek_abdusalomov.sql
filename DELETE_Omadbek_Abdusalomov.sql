-- Remove film from inventory
DELETE FROM inventory WHERE film_id = 1001;

-- Remove rental records for the film
DELETE FROM rental WHERE film_id = 1001;

-- Remove customer records except for Customer and Inventory tables
DELETE FROM payment WHERE customer_id IN (SELECT customer_id FROM customer WHERE first_name = 'Bard' AND last_name = 'Google AI');
DELETE FROM film_actor WHERE actor_id IN (SELECT actor_id FROM actor WHERE first_name = 'Bard' OR last_name = 'Google AI');
DELETE FROM actor WHERE first_name = 'Bard' OR last_name = 'Google AI';
