INSERT INTO film (film_id, title, description, release_year, language_id, rental_duration, rental_rate, length, rating)
VALUES (1001, 'The Shawshank Redemption', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.', 1994, 1, 2, 4.99, 142, 'PG-13');

INSERT INTO actor (actor_id, first_name, last_name)
VALUES (1001, 'Tim', 'Robbins'),
       (1002, 'Morgan', 'Freeman'),
       (1003, 'Bob', 'Gunton');

INSERT INTO film_actor (film_id, actor_id)
VALUES (1001, 1001),
       (1001, 1002),
       (1001, 1003);

INSERT INTO inventory (inventory_id, film_id, store_id)
VALUES (1001, 1001, 1),
       (1002, 1001, 2),
       (1003, 1001, 3);
