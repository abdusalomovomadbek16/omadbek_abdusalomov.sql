ALTER TABLE film
ALTER COLUMN rental_duration SET DEFAULT 3;

ALTER TABLE film
ALTER COLUMN rental_rate SET DEFAULT 9.99;

UPDATE customer
SET first_name = 'Bard',
    last_name = 'Google AI',
    address_id = (SELECT address_id FROM address LIMIT 1)
WHERE customer_id IN (
  SELECT customer_id
  FROM (
    SELECT customer_id, COUNT(*) AS rental_count
    FROM rental
    GROUP BY customer_id
  ) AS rental_counts
  INNER JOIN (
    SELECT customer_id, COUNT(*) AS payment_count
    FROM payment
    GROUP BY customer_id
  ) AS payment_counts
  ON rental_counts.customer_id = payment_counts.customer_id
  WHERE rental_count >= 10 AND payment_count >= 10
);

UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id IN (
  SELECT customer_id
  FROM (
    SELECT customer_id, COUNT(*) AS rental_count
    FROM rental
    GROUP BY customer_id
  ) AS rental_counts
  INNER JOIN (
    SELECT customer_id, COUNT(*) AS payment_count
    FROM payment
    GROUP BY customer_id
  ) AS payment_counts
  ON rental_counts.customer_id = payment_counts.customer_id
  WHERE rental_count >= 10 AND payment_count >= 10
);
